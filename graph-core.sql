-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2015-06-15 15:24:24
-- 服务器版本: 5.5.43-0ubuntu0.14.04.1
-- PHP 版本: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `graph-core`
--

-- --------------------------------------------------------

--
-- 表的结构 `edge`
--

CREATE TABLE IF NOT EXISTS `edge` (
  `edge_id` bigint(16) NOT NULL AUTO_INCREMENT,
  `from_node_id` int(8) NOT NULL,
  `to_node_id` int(8) NOT NULL,
  PRIMARY KEY (`edge_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `edge_attribute_name`
--

CREATE TABLE IF NOT EXISTS `edge_attribute_name` (
  `edge_attribute_name_id` int(8) NOT NULL AUTO_INCREMENT,
  `edge_attribute_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`edge_attribute_name_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `edge_attribute_value`
--

CREATE TABLE IF NOT EXISTS `edge_attribute_value` (
  `edge_attribute_value_id` bigint(18) NOT NULL AUTO_INCREMENT,
  `edge_attribute_name_id` int(8) NOT NULL,
  `edge_id` bigint(16) NOT NULL,
  `edge_attribute_value` varchar(255) NOT NULL,
  PRIMARY KEY (`edge_attribute_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `node`
--

CREATE TABLE IF NOT EXISTS `node` (
  `node_id` int(8) NOT NULL,
  PRIMARY KEY (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 表的结构 `node_attribute_name`
--

CREATE TABLE IF NOT EXISTS `node_attribute_name` (
  `node_attribute_name_id` int(8) NOT NULL AUTO_INCREMENT,
  `node_attribute_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`node_attribute_name_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `node_attribute_value`
--

CREATE TABLE IF NOT EXISTS `node_attribute_value` (
  `node_attribute_value_id` bigint(16) NOT NULL AUTO_INCREMENT,
  `node_attribute_name_id` int(8) NOT NULL,
  `node_id` int(8) NOT NULL,
  `node_attribute_value` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`node_attribute_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
