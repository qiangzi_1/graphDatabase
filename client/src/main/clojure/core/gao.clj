(ns core.gao
  (:use core.utils)
  (:import [com.ufo5260987423.graphDatabase.client.utils GraphNode GraphEdge]))

(defn java-clojure-map-convert [target]
  (reduce #(conj %1 {%2 (.get target %2)}) {} (keys target)))

(defn java-clojure-for-graph-edge [graph-edge-target]
  (if (nil? graph-edge-target)
    ()
    (graph-edge
      (.getEdgeId graph-edge-target)
      (into {} (.getAttributeMap graph-edge-target))
      (.getFromNodeId graph-edge-target)
      (.getToNodeId graph-edge-target))))

(defn clojure-java-for-graph-edge [graph-edge-target]
  (if (nil? graph-edge-target)
    nil
    (let [result (new GraphEdge)]
      (.setEdgeId result (:graph-edge-id graph-edge-target))
      (.setAttributeMap result (java.util.HashMap. (:graph-edge-attribute-map graph-edge-target)))
      (.setFromNodeId result (Integer. (:from-node-id graph-edge-target)))
      (.setToNodeId result (Integer. (:to-node-id graph-edge-target)))
      result)))

(defn java-clojure-for-graph-node [graph-node-target]
  (if (nil? graph-node-target)
    nil
    (graph-node
      (.getGraphNodeId graph-node-target)
      (into {} (.getAttributeMap graph-node-target))
      (reduce #(conj %1 {(first %2) (java-clojure-for-graph-edge (second %2))})
        {} (java-clojure-map-convert (.getFromThisNodeEdges graph-node-target)))
      (reduce #(conj %1 {(first %2) (java-clojure-for-graph-edge (second %2))})
        {} (java-clojure-map-convert (.getToThisNodeEdges graph-node-target))))))

(defn clojure-java-for-graph-node [graph-node-target]
  (if (nil? graph-node-target)
    nil
    (let [result (new GraphNode)]
      (.setGraphNodeId result (Integer. (:graph-node-id graph-node-target)))
      (.setAttributeMap result (java.util.HashMap. (:graph-node-attribute-map graph-node-target)))
      (.setFromThisNodeEdges result
        (java.util.HashMap. (reduce #(conj %1 {(first %2) (clojure-java-for-graph-edge (second %2))})
          {} (:from-this-node-edges-map graph-node-target))))
      (.setToThisNodeEdges result
        (java.util.HashMap. (reduce #(conj %1 {(first %2) (clojure-java-for-graph-edge (second %2))})
          {} (:to-this-node-edges-map graph-node-target))))
      result)))
