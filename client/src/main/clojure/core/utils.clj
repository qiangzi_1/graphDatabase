(ns core.utils)

(println "load core.utils")

(defn server [ip port] {:ip ip :port port})
(defn graph-node [graph-node-id graph-node-attribute-map from-this-node-edges-map to-this-node-edges-map]
  {:graph-node-id graph-node-id
   :graph-node-attribute-map graph-node-attribute-map
   :from-this-node-edges-map from-this-node-edges-map
   :to-this-node-edges-map to-this-node-edges-map})
(defn graph-edge [graph-edge-id graph-edge-attribute-map from-node-id to-node-id]
  {:graph-edge-id graph-edge-id
   :graph-edge-attribute-map graph-edge-attribute-map
   :from-node-id from-node-id
   :to-node-id to-node-id})

(defn eval-string [target]
  (if (or (nil? target) (= "" target))
    nil
    (try
      (eval (read-string target))
      (catch Exception e
        (do
          (println e)
          nil)))))

(defn parse_address [target]
  (apply #(vec [%1 (Integer/parseInt %2)])
    (seq (.split target ":"))))

(defn promise? [p]
  (instance? clojure.lang.IPending p))