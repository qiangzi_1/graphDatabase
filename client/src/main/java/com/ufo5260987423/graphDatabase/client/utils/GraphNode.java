package com.ufo5260987423.graphDatabase.client.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ufo on 5/18/15.
 */
public class GraphNode {
    private Integer graphNodeId;
    private Map<String, String> attributeMap = new HashMap<String, String>();
    private Map<Integer, GraphEdge> fromThisNodeEdges = new HashMap<Integer, GraphEdge>();
    private Map<Integer, GraphEdge> toThisNodeEdges = new HashMap<Integer, GraphEdge>();

    public Map<Integer, GraphEdge> getToThisNodeEdges() {
        return toThisNodeEdges;
    }

    public void setToThisNodeEdges(Map<Integer, GraphEdge> toThisNodeEdges) {
        this.toThisNodeEdges = toThisNodeEdges;
    }

    public Map<Integer, GraphEdge> getFromThisNodeEdges() {
        return fromThisNodeEdges;
    }

    public void setFromThisNodeEdges(Map<Integer, GraphEdge> fromThisNodeEdges) {
        this.fromThisNodeEdges = fromThisNodeEdges;
    }

    public Integer getGraphNodeId() {
        return graphNodeId;
    }

    public void setGraphNodeId(Integer graphNodeId) {
        this.graphNodeId = graphNodeId;
    }

    public Map<String, String> getAttributeMap() {
        return attributeMap;
    }

    public void setAttributeMap(Map<String, String> attributeMap) {
        this.attributeMap = attributeMap;
    }
}
