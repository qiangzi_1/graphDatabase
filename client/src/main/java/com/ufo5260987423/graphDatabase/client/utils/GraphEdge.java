package com.ufo5260987423.graphDatabase.client.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ufo on 5/18/15.
 */
public class GraphEdge {
    private Long edgeId;
    private Integer fromNodeId;
    private Integer toNodeId;
    private Map<String, String> attributeMap = new HashMap<String, String>();

    public Long getEdgeId() {
        return edgeId;
    }

    public void setEdgeId(Long edgeId) {
        this.edgeId = edgeId;
    }

    public Integer getFromNodeId() {
        return fromNodeId;
    }

    public void setFromNodeId(Integer fromNodeId) {
        this.fromNodeId = fromNodeId;
    }

    public Integer getToNodeId() {
        return toNodeId;
    }

    public void setToNodeId(Integer toNodeId) {
        this.toNodeId = toNodeId;
    }

    public Map<String, String> getAttributeMap() {
        return attributeMap;
    }

    public void setAttributeMap(Map<String, String> attributeMap) {
        this.attributeMap = attributeMap;
    }
}
