/**
 * Copyright 2015年1月30日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.GAO;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: GraphNode
 * @Description: TODO
 * @author ufo ufo5260987423@163.com
 * @date 2015年3月27日 下午8:19:54
 * 
 */
public class GraphNode {
	private GraphNodeFactory graphNodeFactory;
	private GraphEdgeFactory graphEdgeFactory;

	private Integer graphNodeId;
	private Map<String, String> attributeMap = new HashMap<String, String>();
	private Map<Integer, GraphEdge> fromThisNodeEdges = new HashMap<Integer, GraphEdge>();
	private Map<Integer, GraphEdge> toThisNodeEdges = new HashMap<Integer, GraphEdge>();

	public void putAttribute(String key, String value) {
		this.getAttributeMap().put(key, value);
	}

	public String getAttribute(String key) {
		return this.getAttributeMap().get(key);
	}

	public void putToThisNodeGraphEdge(GraphEdge graphEdge) throws Exception {
		if(this.getGraphNodeId()!=null&&
				graphEdge.getToNodeId()!=null&&
				graphEdge.getFromNodeId()!=null)
			this.getToThisNodeEdges().put(graphEdge.getFromNodeId(),graphEdge);
		else
			throw new Exception();
	}

	public GraphEdge getToThisNodeGraphEdge(Integer fromNodeId) {
		return this.getToThisNodeEdges().get(fromNodeId);
	}

	public void putFromThisNodeGraphEdge(GraphEdge graphEdge) throws Exception {
		if(this.getGraphNodeId()!=null&&
				graphEdge.getToNodeId()!=null&&
				graphEdge.getFromNodeId()!=null)
			this.getFromThisNodeEdges().put(graphEdge.getToNodeId(),graphEdge);
		else
			throw new Exception();
	}

	public GraphEdge getFromThisNodeGraphEdge(Integer toNodeId) {
		return this.getFromThisNodeEdges().get(toNodeId);
	}

	public GraphEdgeFactory getGraphEdgeFactory() {
		return graphEdgeFactory;
	}

	public void setGraphEdgeFactory(GraphEdgeFactory graphEdgeFactory) {
		this.graphEdgeFactory = graphEdgeFactory;
	}

	public Integer getGraphNodeId() {
		return graphNodeId;
	}

	public void setGraphNodeId(Integer graphNodeId) {
		this.graphNodeId = graphNodeId;
	}

	public Map<String, String> getAttributeMap() {
		return attributeMap;
	}

	public void setAttributeMap(Map<String, String> attributeMap) {
		this.attributeMap = attributeMap;
	}

	public GraphNodeFactory getGraphNodeFactory() {
		return graphNodeFactory;
	}

	public void setGraphNodeFactory(GraphNodeFactory graphNodeFactory) {
		this.graphNodeFactory = graphNodeFactory;
	}

	public Map<Integer, GraphEdge> getFromThisNodeEdges() {
		return fromThisNodeEdges;
	}

	public void setFromThisNodeEdges(Map<Integer, GraphEdge> fromThisNodeEdges) {
		this.fromThisNodeEdges = fromThisNodeEdges;
	}

	public Map<Integer, GraphEdge> getToThisNodeEdges() {
		return toThisNodeEdges;
	}

	public void setToThisNodeEdges(Map<Integer, GraphEdge> toThisNodeEdges) {
		this.toThisNodeEdges = toThisNodeEdges;
	}
}
