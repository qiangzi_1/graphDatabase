/**
 * Copyright 2015年4月7日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.connector;

import clojure.lang.IFn;
import clojure.lang.RT;
import clojure.lang.Symbol;

import com.ufo5260987423.graphDatabase.core.lifecycle.LifeCycleInf;

/**
 * @ClassName: REPL_Connector
 * @Description: TODO
 * @author Wang Zheng ufo5260987423@163.com
 * @date 2015年4月7日 上午8:58:15
 * 
 */
public class REPL_Connector implements LifeCycleInf {

	public static REPL_Connector repl_connector_instance = null;
	private static IFn useFn = RT.var("clojure.core", "use").fn();
	private IFn eval_string = null;
	private String[] servers;
	private String local;

	public String eval_string(String target) throws Exception{
			return this.getEval_string().invoke(target).toString() + "\r";
	}

	@Override
	public Boolean loadConf() {
		System.err.println("repl_connector loading");
		repl_connector_instance = this;

		useFn.invoke(Symbol.intern("apis.basic"));
        useFn.invoke(Symbol.intern("core.client"));
        useFn.invoke(Symbol.intern("core.gao"));
        useFn.invoke(Symbol.intern("core.utils"));
        useFn.invoke(Symbol.intern("core.route"));

		this.setEval_string(RT.var("core.utils", "eval-string"));

		return null;
	}

	@Override
	public void run() {
		System.err.println("run repl_connector");
		try {
			this.eval_string("(init)");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public IFn getEval_string() {
		return eval_string;
	}

	public void setEval_string(IFn eval_string) {
		this.eval_string = eval_string;
	}

	public String[] getServers() {
		return servers;
	}

	public void setServers(String[] servers) {
		this.servers = servers;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

}
