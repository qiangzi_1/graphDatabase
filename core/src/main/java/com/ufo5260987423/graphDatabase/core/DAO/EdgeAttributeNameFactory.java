/**
 * Copyright 2015年1月30日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ufo5260987423.graphDatabase.core.sqlTypes.SqlChars;
import com.ufo5260987423.graphDatabase.core.sqlTypes.SqlInteger;

/**
 * @ClassName: EdgeAttributeNameFactory
 * @Description: TODO
 * @author ufo ufo5260987423@163.com
 * @date 2015年3月27日 下午4:12:56
 * 
 */
public class EdgeAttributeNameFactory extends AbstractFactory<EdgeAttributeName, SqlInteger<Integer>> {

	public EdgeAttributeNameFactory() {
		this.init();
	}

	public EdgeAttributeName getBy(String nameString) {
		try {
			List<EdgeAttributeName> list = this.select("where " + EDGE_ATTRIBUTE_NAME + "='" + nameString + "'");
			if (list.size() == 0)
				return null;
			else
				return list.get(0);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getTable() {
		return "" + EDGE_ATTRIBUTE_NAME + "";
	}

	@Override
	public EdgeAttributeName insert(EdgeAttributeName a) throws SQLException {
		String sql = "insert into " + this.getTable() + " (" + EDGE_ATTRIBUTE_NAME_ID + "," + EDGE_ATTRIBUTE_NAME
				+ ") values(NULL,?)";
		PreparedStatement ps = this.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, a.getEdgeAttributeName().getContent());
		ps.executeUpdate();
		this.getConnection().commit();
		ResultSet rs = ps.getGeneratedKeys();
		while (rs.next())
			a.setId(new SqlInteger<Integer>((short) 8, rs.getInt(1)));
		rs.close();
		ps.close();
		return a;
	}

	@Override
	public void update(EdgeAttributeName a) throws SQLException {
		String sql = "update " + this.getTable() + " set  " + EDGE_ATTRIBUTE_NAME + "=? where "
				+ EDGE_ATTRIBUTE_NAME_ID + "=?";
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		ps.setString(1, a.getEdgeAttributeName().getContent());
		ps.setInt(2, a.getId().getContent());
		ps.execute();
		this.getConnection().commit();
		ps.close();
	}

	@Override
	public void delete(EdgeAttributeName a) throws SQLException {
		String sql = "delete from " + this.getTable() + " where " + EDGE_ATTRIBUTE_NAME_ID + "=?";
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		ps.setInt(1, a.getId().getContent());
		ps.execute();
		ps.close();
		this.getConnection().commit();
	}

	@Override
	public List<EdgeAttributeName> select(String where) throws SQLException {
		List<EdgeAttributeName> result = new ArrayList<EdgeAttributeName>();
		String sql = "select " + EDGE_ATTRIBUTE_NAME_ID + "," + EDGE_ATTRIBUTE_NAME + " from " + this.getTable() + " "
				+ where;
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			EdgeAttributeName edgeAttributeName = new EdgeAttributeName(new SqlChars<String>((short) 255,
					rs.getString("" + EDGE_ATTRIBUTE_NAME + "")));
			edgeAttributeName.setId(new SqlInteger<Integer>((short) 8, rs.getInt("" + EDGE_ATTRIBUTE_NAME_ID + "")));
			result.add(edgeAttributeName);
		}
		rs.close();
		ps.close();
		return result;
	}

	@Override
	public void init() {
		super.init();
		String sql = "CREATE TABLE IF NOT EXISTS `" + this.getTable() + "` (`" + EDGE_ATTRIBUTE_NAME_ID
				+ "` int(8) NOT NULL AUTO_INCREMENT," + "`" + EDGE_ATTRIBUTE_NAME
				+ "` varchar(255) CHARACTER SET utf8 NOT NULL," + "PRIMARY KEY (`" + EDGE_ATTRIBUTE_NAME_ID
				+ "`)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
		try {
			PreparedStatement ps = this.getConnection().prepareStatement(sql);
			ps.execute();
			ps.close();
			this.getConnection().commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public EdgeAttributeName getBean(SqlInteger<Integer> id) throws SQLException {
		List<EdgeAttributeName> list = this.select("where " + EDGE_ATTRIBUTE_NAME_ID + "=" + id.getContent());
		if (list.size() == 0)
			return null;
		else
			return list.get(0);
	}
}
