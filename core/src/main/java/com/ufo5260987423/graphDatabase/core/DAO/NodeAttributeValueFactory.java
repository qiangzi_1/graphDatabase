/**
 * Copyright 2015年1月30日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ufo5260987423.graphDatabase.core.sqlTypes.SqlChars;
import com.ufo5260987423.graphDatabase.core.sqlTypes.SqlInteger;

/**
 * @ClassName: NodeAttributeValueFactory
 * @Description: TODO
 * @author ufo ufo5260987423@163.com
 * @date 2015年3月27日 下午1:17:53
 * 
 */
public class NodeAttributeValueFactory extends AbstractFactory<NodeAttributeValue, SqlInteger<Long>> {

	public NodeAttributeValueFactory() {
		this.init();
	}

	public NodeAttributeValue getBy(Integer nodeId, Integer nodeAttributeNameId) {
		String where = "where " + NODE_ID + "=" + nodeId + " and " + NODE_ATTRIBUTE_NAME_ID + "=" + nodeAttributeNameId;
		try {
			List<NodeAttributeValue> list = this.select(where);
			if (list.size() == 0)
				return null;
			else
				return list.get(0);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getTable() {
		return "" + NODE_ATTRIBUTE_VALUE + "";
	}

	@Override
	public NodeAttributeValue insert(NodeAttributeValue a) throws SQLException {
		String sql = "insert into " + this.getTable() + "(" + NODE_ATTRIBUTE_VALUE_ID + "," + NODE_ATTRIBUTE_NAME_ID
				+ "," + NODE_ID + "," + NODE_ATTRIBUTE_VALUE + ") values(NULL,?,?,?)";
		PreparedStatement ps = this.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		ps.setInt(1, a.getNodeAttributeNameId().getContent());
		ps.setInt(2, a.getNodeId().getContent());
		ps.setString(3, a.getNodeAttributeValue().getContent());
		ps.executeUpdate();
		this.getConnection().commit();
		ResultSet rs = ps.getGeneratedKeys();
		while (rs.next())
			a.setId(new SqlInteger<Long>((short) 24, rs.getLong(1)));
		rs.close();
		ps.close();
		return a;
	}

	@Override
	public void update(NodeAttributeValue a) throws SQLException {
		String sql = "update " + this.getTable() + " set " + NODE_ATTRIBUTE_VALUE + "=?" + " where "
				+ NODE_ATTRIBUTE_VALUE_ID + "=?";
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		ps.setString(1, a.getNodeAttributeValue().getContent());
		ps.setLong(2, a.getId().getContent());
		ps.execute();
		this.getConnection().commit();
		ps.close();
	}

	@Override
	public void delete(NodeAttributeValue a) throws SQLException {
		String sql = "delete from " + this.getTable() + " where " + NODE_ATTRIBUTE_VALUE_ID + "=?";
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		ps.setLong(1, a.getId().getContent());
		ps.execute();
		this.getConnection().commit();
		ps.close();
	}

	@Override
	public List<NodeAttributeValue> select(String where) throws SQLException {
		List<NodeAttributeValue> result = new ArrayList<NodeAttributeValue>();
		String sql = "select " + NODE_ATTRIBUTE_VALUE_ID + "," + NODE_ATTRIBUTE_NAME_ID + "," + NODE_ATTRIBUTE_VALUE
				+ "," + NODE_ID + " from " + this.getTable() + " " + where;
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			NodeAttributeValue nodeAttributeValue = new NodeAttributeValue(new SqlChars<String>((short) 255,
					rs.getString("" + NODE_ATTRIBUTE_VALUE + "")), new SqlInteger<Integer>((short) 8, rs.getInt(""
					+ NODE_ID + "")), new SqlInteger<Integer>((short) 8, rs.getInt("" + NODE_ATTRIBUTE_NAME_ID + "")));
			nodeAttributeValue.setId(new SqlInteger<Long>((short) 18, rs.getLong("" + NODE_ATTRIBUTE_VALUE_ID + "")));

			result.add(nodeAttributeValue);
		}
		rs.close();
		ps.close();
		return result;
	}

	@Override
	public void init() {
		super.init();
		String sql = "CREATE TABLE IF NOT EXISTS `" + this.getTable() + "` (" + "`" + NODE_ATTRIBUTE_VALUE_ID
				+ "` bigint(16) NOT NULL AUTO_INCREMENT," + "`" + NODE_ATTRIBUTE_NAME_ID + "` int(8) NOT NULL," + "`"
				+ NODE_ID + "` int(8) NOT NULL," + "`" + NODE_ATTRIBUTE_VALUE
				+ "` varchar(255) CHARACTER SET utf8 NOT NULL," + "PRIMARY KEY (`" + NODE_ATTRIBUTE_VALUE_ID + "`)"
				+ ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		try {
			PreparedStatement ps = this.getConnection().prepareStatement(sql);
			ps.execute();
			this.getConnection().commit();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public NodeAttributeValue getBean(SqlInteger<Long> id) throws SQLException {
		List<NodeAttributeValue> list = this.select("where " + NODE_ATTRIBUTE_VALUE_ID + "=" + id.getContent());
		if (list.size() == 0)
			return null;
		else
			return list.get(0);
	}
}
