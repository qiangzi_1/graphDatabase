/**
 * Copyright 2015年3月27日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.GAO;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: GraphEdge
 * @Description: TODO
 * @author ufo ufo5260987423@163.com
 * @date 2015年3月27日 下午8:55:03
 * 
 */
public class GraphEdge {
	private GraphNodeFactory graphNodeFactory;
	private GraphEdgeFactory graphEdgeFactory;

	private Long edgeId;
	private Integer fromNodeId;
	private Integer toNodeId;
	private Map<String, String> attributeMap = new HashMap<String, String>();

	public GraphNode getFromNode() throws Exception {
		return this.getGraphNodeFactory().getGraphNodeBy(this.getFromNodeId());
	}

	public GraphNode getToNode() throws Exception {
		return this.getGraphNodeFactory().getGraphNodeBy(this.getToNodeId());
	}

	public String getAttribute(String keyString) {
		return this.getAttributeMap().get(keyString);
	}

	public void setAttribute(String keyString, String valueString) {
		this.getAttributeMap().put(keyString, valueString);
	}

	public Long save() {
		if (null != this.getEdgeId())
			return this.getEdgeId();
		else {
			return null;
		}
	}

	public Map<String, String> getAttributeMap() {
		return attributeMap;
	}

	public void setAttributeMap(Map<String, String> attributeMap) {
		this.attributeMap = attributeMap;
	}

	public GraphNodeFactory getGraphNodeFactory() {
		return graphNodeFactory;
	}

	public void setGraphNodeFactory(GraphNodeFactory graphNodeFactory) {
		this.graphNodeFactory = graphNodeFactory;
	}

	public GraphEdgeFactory getGraphEdgeFactory() {
		return graphEdgeFactory;
	}

	public void setGraphEdgeFactory(GraphEdgeFactory graphEdgeFactory) {
		this.graphEdgeFactory = graphEdgeFactory;
	}

	public Long getEdgeId() {
		return edgeId;
	}

	public void setEdgeId(Long edgeId) {
		this.edgeId = edgeId;
	}

	public Integer getFromNodeId() {
		return fromNodeId;
	}

	public void setFromNodeId(Integer fromNodeId) {
		this.fromNodeId = fromNodeId;
	}

	public Integer getToNodeId() {
		return toNodeId;
	}

	public void setToNodeId(Integer toNodeId) {
		this.toNodeId = toNodeId;
	}

}
