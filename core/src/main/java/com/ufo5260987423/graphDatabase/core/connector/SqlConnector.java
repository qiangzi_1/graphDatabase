/**
 * Copyright 2015年1月30日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.connector;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.ufo5260987423.graphDatabase.core.DAO.AbstractFactory;
import com.ufo5260987423.graphDatabase.core.lifecycle.LifeCycleInf;

/**
 * @ClassName: SqlConnector
 * @Description: TODO
 * @author ufo ufo5260987423@163.com
 * @date 2015年3月25日 下午9:21:39
 * 
 */
public class SqlConnector implements LifeCycleInf {

	private String user;
	private String password;
	private String driverClass;
	private String jdbcUrl;
	private Integer initialPoolSize;
	private Integer maxIdleTime;
	private Integer maxPoolSize;
	private Integer minPoolSize;

	private static ComboPooledDataSource cpds = new ComboPooledDataSource();

	public Connection getConnection() throws SQLException {
		return this.getCpds().getConnection();
	}

	public ComboPooledDataSource getCpds() {
		return cpds;
	}

	public void setCpds(ComboPooledDataSource cpds) {
		SqlConnector.cpds = cpds;
	}

	@Override
	public Boolean loadConf() {
		System.err.println("sqlConnector loading");
		if (null == this.getUser())
			return false;
		this.getCpds().setUser(this.getUser());
		this.getCpds().setPassword(this.getPassword());
		try {
			this.getCpds().setDriverClass(this.getDriverClass());
		} catch (PropertyVetoException e) {
			e.printStackTrace();
			return false;
		}
		this.getCpds().setJdbcUrl(this.getJdbcUrl());
		this.getCpds().setInitialPoolSize(this.getInitialPoolSize());
		this.getCpds().setMaxIdleTime(this.getMaxIdleTime());
		this.getCpds().setMaxPoolSize(this.getMaxPoolSize());
		this.getCpds().setMinPoolSize(this.getMinPoolSize());
		this.setCpds(cpds);

		AbstractFactory.CONNECTION_FACTORY = this;
		// sqlConnector_instance=this;
		return true;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDriverClass() {
		return driverClass;
	}

	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}

	public String getJdbcUrl() {
		return jdbcUrl;
	}

	public void setJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
	}

	public Integer getInitialPoolSize() {
		return initialPoolSize;
	}

	public void setInitialPoolSize(Integer initialPoolSize) {
		this.initialPoolSize = initialPoolSize;
	}

	public Integer getMaxIdleTime() {
		return maxIdleTime;
	}

	public void setMaxIdleTime(Integer maxIdleTime) {
		this.maxIdleTime = maxIdleTime;
	}

	public Integer getMaxPoolSize() {
		return maxPoolSize;
	}

	public void setMaxPoolSize(Integer maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}

	public Integer getMinPoolSize() {
		return minPoolSize;
	}

	public void setMinPoolSize(Integer minPoolSize) {
		this.minPoolSize = minPoolSize;
	}

	public void run() {
		System.err.println("run sq1Connector");
	}
}
