/**
 * Copyright 2015年4月2日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.lifecycle;

/**
 * @ClassName: LifeCycleInf
 * @Description: TODO
 * @author Wang Zheng ufo5260987423@163.com
 * @date 2015年4月2日 下午3:22:00
 * 
 */
public interface LifeCycleInf {
	public Boolean loadConf();

	public void run();
}
