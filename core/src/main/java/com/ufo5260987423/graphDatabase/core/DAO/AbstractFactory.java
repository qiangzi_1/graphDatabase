/**
 * Copyright 2015年1月30日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.DAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.ufo5260987423.graphDatabase.core.connector.SqlConnector;
import com.ufo5260987423.graphDatabase.core.sqlTypes.AbstractAutoIncrementableSqlType;

/**
 * @ClassName: AbstractFactory
 * @Description: TODO
 * @author ufo ufo5260987423@163.com
 * @date 2015年3月25日 下午10:30:23
 * 
 */
public abstract class AbstractFactory<Bean extends AbstractBean<?>, IdType extends AbstractAutoIncrementableSqlType<? extends Number>> {

	public static final String NODE_ID = "node_id";

	public static final String NODE_ATTRIBUTE_NAME_ID = "node_attribute_name_id";
	public static final String NODE_ATTRIBUTE_NAME = "node_attribute_name";
	public static final String NODE_ATTRIBUTE_VALUE_ID = "node_attribute_value_id";
	public static final String NODE_ATTRIBUTE_VALUE = "node_attribute_value";

	public static final String EDGE_ID = "edge_id";
	public static final String FROM_NODE_ID = "from_node_id";
	public static final String TO_NODE_ID = "to_node_id";

	public static final String EDGE_ATTRIBUTE_NAME_ID = "edge_attribute_name_id";
	public static final String EDGE_ATTRIBUTE_NAME = "edge_attribute_name";
	public static final String EDGE_ATTRIBUTE_VALUE_ID = "edge_attribute_value_id";
	public static final String EDGE_ATTRIBUTE_VALUE = "edge_attribute_value";

	public static SqlConnector CONNECTION_FACTORY;
	private Connection connection;

	public Boolean isExist(IdType id) {
		try {
			return null != this.getBean(id);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public void rollback() {
		try {
			this.getConnection().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void init() {
		try {
			this.setConnection(CONNECTION_FACTORY.getConnection());
			this.getConnection().setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public AbstractFactory() {
		try {
			this.setConnection(CONNECTION_FACTORY.getConnection());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public abstract String getTable();

	public String getScheme() {
		try {
			return this.getConnection().getSchema();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public abstract Bean insert(Bean a) throws SQLException;

	public abstract void update(Bean a) throws SQLException;

	public abstract void delete(Bean a) throws SQLException;

	public abstract List<Bean> select(String where) throws SQLException;

	public abstract Bean getBean(IdType id) throws SQLException;

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public void finnalize() throws Throwable {
		if (this.getConnection() != null)
			this.getConnection().close();
		super.finalize();
	}
}