/**
 * Copyright 2015年1月30日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ufo5260987423.graphDatabase.core.sqlTypes.SqlChars;
import com.ufo5260987423.graphDatabase.core.sqlTypes.SqlInteger;

/**
 * @ClassName: EdgeAttributeValueFactory
 * @Description: TODO
 * @author ufo ufo5260987423@163.com
 * @date 2015年3月27日 下午6:05:40
 * 
 */
public class EdgeAttributeValueFactory extends AbstractFactory<EdgeAttributeValue, SqlInteger<Long>> {

	public EdgeAttributeValueFactory() {
		this.init();
	}

	@Override
	public String getTable() {
		return "" + EDGE_ATTRIBUTE_VALUE + "";
	}

	public EdgeAttributeValue getBy(Long edgeId, Integer edgeAttributeNameId) {
		try {
			List<EdgeAttributeValue> list = this.select("where " + EDGE_ID + "=" + edgeId + " and "
					+ EDGE_ATTRIBUTE_NAME_ID + "=" + edgeAttributeNameId);
			if (list.size() == 0)
				return null;
			else
				return list.get(0);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public EdgeAttributeValue insert(EdgeAttributeValue a) throws SQLException {
		String sql = "insert into " + this.getTable() + " (" + EDGE_ATTRIBUTE_VALUE_ID + "," + EDGE_ATTRIBUTE_NAME_ID
				+ "," + EDGE_ID + "," + EDGE_ATTRIBUTE_VALUE + ") values(NULL,?,?,?)";
		PreparedStatement ps = this.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		ps.setInt(1, a.getEdgeAttributeNameId().getContent());
		ps.setLong(2, a.getEdgeId().getContent());
		ps.setString(3, a.getEdgeAttributeValue().getContent());
		ps.execute();
		this.getConnection().commit();
		ResultSet rs = ps.getGeneratedKeys();
		while (rs.next())
			a.setId(new SqlInteger<Long>((short) 24, rs.getLong(1)));
		rs.close();
		ps.close();
		return a;
	}

	@Override
	public void update(EdgeAttributeValue a) throws SQLException {
		String sql = "update " + this.getTable() + " set " + EDGE_ATTRIBUTE_VALUE + "=?" + " where "
				+ EDGE_ATTRIBUTE_VALUE_ID + "=?";
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		ps.setString(1, a.getEdgeAttributeValue().getContent());
		ps.setLong(2, a.getId().getContent());
		ps.execute();
		this.getConnection().commit();
		ps.close();
	}

	@Override
	public void delete(EdgeAttributeValue a) throws SQLException {
		String sql = "delete from " + this.getTable() + " where " + EDGE_ATTRIBUTE_VALUE_ID + "=?";
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		System.out.println(a.getId());
		ps.setLong(1, a.getId().getContent());
		ps.execute();
		this.getConnection().commit();
		ps.close();
	}

	@Override
	public List<EdgeAttributeValue> select(String where) throws SQLException {
		List<EdgeAttributeValue> result = new ArrayList<EdgeAttributeValue>();
		String sql = "select " + EDGE_ATTRIBUTE_VALUE_ID + "," + EDGE_ATTRIBUTE_NAME_ID + "," + EDGE_ID + ","
				+ EDGE_ATTRIBUTE_VALUE + " from " + this.getTable() + " " + where;
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			EdgeAttributeValue edgeAttributeValue = new EdgeAttributeValue(new SqlInteger<Integer>((short) 8,
					rs.getInt("" + EDGE_ATTRIBUTE_NAME_ID + "")), new SqlInteger<Long>((short) 16, rs.getLong(""
					+ EDGE_ID + "")), new SqlChars<String>((short) 255, rs.getString("" + EDGE_ATTRIBUTE_VALUE + "")));
			edgeAttributeValue.setId(new SqlInteger<Long>((short) 24, rs.getLong("" + EDGE_ATTRIBUTE_VALUE_ID + "")));
			result.add(edgeAttributeValue);
		}
		return result;
	}

	@Override
	public void init() {
		super.init();
		String sql = "CREATE TABLE IF NOT EXISTS `" + this.getTable() + "` (" + "`" + EDGE_ATTRIBUTE_VALUE_ID
				+ "` bigint(18) NOT NULL AUTO_INCREMENT," + "`" + EDGE_ATTRIBUTE_NAME_ID + "` int(8) NOT NULL," + "`"
				+ EDGE_ID + "` bigint(16) NOT NULL," + "`" + EDGE_ATTRIBUTE_VALUE + "` varchar(255) NOT NULL,"
				+ "PRIMARY KEY (`" + EDGE_ATTRIBUTE_VALUE_ID + "`)"
				+ ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		try {
			PreparedStatement ps = this.getConnection().prepareStatement(sql);
			ps.execute();
			ps.close();
			this.getConnection().commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public EdgeAttributeValue getBean(SqlInteger<Long> id) throws SQLException {
		List<EdgeAttributeValue> list = this.select("where " + EDGE_ATTRIBUTE_VALUE_ID + "=" + id.getContent());
		if (list.size() == 0)
			return null;
		else
			return list.get(0);
	}
}
