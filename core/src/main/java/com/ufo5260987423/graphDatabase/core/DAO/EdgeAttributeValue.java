/**
 * Copyright 2015年1月30日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.DAO;

import com.ufo5260987423.graphDatabase.core.sqlTypes.SqlChars;
import com.ufo5260987423.graphDatabase.core.sqlTypes.SqlInteger;

/**
 * @ClassName: EdgeAttributeValue
 * @Description: TODO
 * @author ufo ufo5260987423@163.com
 * @date 2015年3月27日 下午5:51:27
 * 
 */
public class EdgeAttributeValue extends AbstractBean<SqlInteger<Long>> {
	private SqlInteger<Integer> edgeAttributeNameId;
	private SqlInteger<Long> edgeId;
	private SqlChars<String> edgeAttributeValue;

	public EdgeAttributeValue() {
	}

	public EdgeAttributeValue(SqlInteger<Integer> edgeAttributeNameId, SqlInteger<Long> edgeId,
			SqlChars<String> edgeAttributeValue) {
		this.setEdgeAttributeNameId(edgeAttributeNameId);
		this.setEdgeAttributeValue(edgeAttributeValue);
		this.setEdgeId(edgeId);
	}

	public SqlInteger<Integer> getEdgeAttributeNameId() {
		return edgeAttributeNameId;
	}

	public void setEdgeAttributeNameId(SqlInteger<Integer> edgeAttributeNameId) {
		this.edgeAttributeNameId = edgeAttributeNameId;
	}

	public SqlChars<String> getEdgeAttributeValue() {
		return edgeAttributeValue;
	}

	public void setEdgeAttributeValue(SqlChars<String> edgeAttributeValue) {
		this.edgeAttributeValue = edgeAttributeValue;
	}

	public SqlInteger<Long> getEdgeId() {
		return edgeId;
	}

	public void setEdgeId(SqlInteger<Long> edgeId) {
		this.edgeId = edgeId;
	}
}
