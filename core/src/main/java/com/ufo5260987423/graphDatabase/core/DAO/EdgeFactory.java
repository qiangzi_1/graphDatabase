/**
 * Copyright 2015年1月30日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ufo5260987423.graphDatabase.core.sqlTypes.SqlInteger;

/**
 * @ClassName: EdgeFactory
 * @Description: TODO
 * @author ufo ufo5260987423@163.com
 * @date 2015年3月27日 下午2:24:58
 * 
 */
public class EdgeFactory extends AbstractFactory<Edge, SqlInteger<Long>> {

	public EdgeFactory() {
		this.init();
	}

	@Override
	public String getTable() {
		return "edge";
	}

	@Override
	public Edge insert(Edge a) throws SQLException {
		String sql = "insert into " + this.getTable() + " (" + EDGE_ID + "," + FROM_NODE_ID + "," + TO_NODE_ID
				+ ") values(NULL,?,?)";
		PreparedStatement ps = this.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		ps.setInt(1, a.getFromNodeId().getContent());
		ps.setInt(2, a.getToNodeId().getContent());
		ps.executeUpdate();
        this.getConnection().commit();
		ResultSet rs = ps.getGeneratedKeys();
		while (rs.next())
			a.setId(new SqlInteger<Long>((short) 16, rs.getLong(1)));
		rs.close();
		ps.close();
		return a;
	}

	@Override
	public void update(Edge a) throws SQLException {
		String sql = "update " + this.getTable() + " set " + FROM_NODE_ID + "=?," + TO_NODE_ID + "=? where " + EDGE_ID
				+ "=?";
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		ps.setInt(1, a.getFromNodeId().getContent());
		ps.setInt(2, a.getToNodeId().getContent());
		ps.setLong(3, a.getId().getContent());
		ps.execute();
		this.getConnection().commit();
		ps.close();
	}

	@Override
	public void delete(Edge a) throws SQLException {
		String sql = "delete from " + this.getTable() + " where " + EDGE_ID + "=?";
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		ps.setLong(1, a.getId().getContent());
		ps.execute();
		this.getConnection().commit();
		ps.close();
	}

	@Override
	public List<Edge> select(String where) throws SQLException {
		List<Edge> result = new ArrayList<Edge>();
		String sql = "select " + EDGE_ID + "," + FROM_NODE_ID + "," + TO_NODE_ID + " from " + this.getTable() + " "
				+ where;
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Edge edge = new Edge(new SqlInteger<Integer>((short) 8, rs.getInt("" + FROM_NODE_ID + "")),
					new SqlInteger<Integer>((short) 8, rs.getInt("" + TO_NODE_ID + "")));
			edge.setId(new SqlInteger<Long>((short) 16, rs.getLong("" + EDGE_ID + "")));
			result.add(edge);
		}
		rs.close();
		ps.close();
		return result;
	}

	@Override
	public void init() {
		super.init();
		String sql = "CREATE TABLE IF NOT EXISTS `" + this.getTable() + "` (" + "`" + EDGE_ID
				+ "` bigint(16) NOT NULL AUTO_INCREMENT," + "`" + FROM_NODE_ID + "` int(8) NOT NULL," + "`" + TO_NODE_ID
				+ "` int(8) NOT NULL,PRIMARY KEY (`" + EDGE_ID + "`)"
				+ ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		try {
			PreparedStatement ps = this.getConnection().prepareStatement(sql);
			ps.execute();
			ps.close();
			this.getConnection().commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Edge getBean(SqlInteger<Long> id) throws SQLException {
		List<Edge> list = this.select("where " + EDGE_ID + "=" + id.getContent());
		if (list.size() == 0)
			return null;
		else
			return list.get(0);
	}
}