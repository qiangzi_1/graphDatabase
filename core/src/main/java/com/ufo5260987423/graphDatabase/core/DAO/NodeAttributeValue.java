/**
 * Copyright 2015年1月30日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.DAO;

import com.ufo5260987423.graphDatabase.core.sqlTypes.SqlChars;
import com.ufo5260987423.graphDatabase.core.sqlTypes.SqlInteger;

/**
 * @ClassName: NodeAttributeValue
 * @Description: TODO
 * @author ufo ufo5260987423@163.com
 * @date 2015年3月27日 下午12:21:50
 * 
 */
public class NodeAttributeValue extends AbstractBean<SqlInteger<Long>> {

	private SqlChars<String> nodeAttributeValue;
	private SqlInteger<Integer> nodeId;
	private SqlInteger<Integer> nodeAttributeNameId;

	public NodeAttributeValue(SqlChars<String> nodeAttributeValue, SqlInteger<Integer> nodeId,
			SqlInteger<Integer> nodeAttributeNameId) {
		this.setNodeAttributeNameId(nodeAttributeNameId);
		this.setNodeAttributeValue(nodeAttributeValue);
		this.setNodeId(nodeId);
	}

	public SqlChars<String> getNodeAttributeValue() {
		return nodeAttributeValue;
	}

	public void setNodeAttributeValue(SqlChars<String> nodeAttributeValue) {
		this.nodeAttributeValue = nodeAttributeValue;
	}

	public SqlInteger<Integer> getNodeId() {
		return nodeId;
	}

	public void setNodeId(SqlInteger<Integer> nodeId) {
		this.nodeId = nodeId;
	}

	public SqlInteger<Integer> getNodeAttributeNameId() {
		return nodeAttributeNameId;
	}

	public void setNodeAttributeNameId(SqlInteger<Integer> nodeAttributeNameId) {
		this.nodeAttributeNameId = nodeAttributeNameId;
	}
}
