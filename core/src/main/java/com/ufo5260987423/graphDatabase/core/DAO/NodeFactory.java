/**
 * Copyright 2015年1月30日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ufo5260987423.graphDatabase.core.sqlTypes.SqlInteger;

/**
 * @ClassName: NodeFactory
 * @Description: TODO
 * @author ufo ufo5260987423@163.com
 * @date 2015年3月26日 下午5:49:07
 * 
 */
public class NodeFactory extends AbstractFactory<Node, SqlInteger<Integer>> {

	public NodeFactory() {
		this.init();
	}

	public Integer getBiggestId() {
		try {
			String sql = "select max(" + NODE_ID + ") from " + this.getTable();
			PreparedStatement ps = this.getConnection().prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				return rs.getInt("max(node_id)");

			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Node insert(Node a) throws SQLException {
		String sql = "insert into " + this.getTable() + " (" + NODE_ID + ") values("+a.getId()+");";
		PreparedStatement ps = this.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		ps.executeUpdate();
		this.getConnection().commit();
		ResultSet rs = ps.getGeneratedKeys();
		while (rs.next())
			a.setId(new SqlInteger<Integer>((short) 8, rs.getInt(1)));
		rs.close();
		ps.close();
		return a;
	}

	@Override
	public void update(Node a) {
	}

	@Override
	public void delete(Node a) throws SQLException {
		String sql = "delete from " + this.getTable() + " where " + NODE_ID + " = ? ;";
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		ps.setInt(1, a.getId().getContent());
		ps.execute();
		this.getConnection().commit();
		ps.close();
	}

	@Override
	public List<Node> select(String where) throws SQLException {
		List<Node> result = new ArrayList<Node>();
		String sql = "select " + NODE_ID + " from " + this.getTable() + " " + where;
		PreparedStatement ps = this.getConnection().prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			Node tmp = new Node(new SqlInteger<Integer>((short) 8, rs.getInt("" + NODE_ID + "")));
			result.add(tmp);
		}
		rs.close();
		ps.close();
		return result;
	}

	@Override
	public String getTable() {
		return "node";
	}

	@Override
	public void init() {
		super.init();
		String sql = "CREATE TABLE IF NOT EXISTS `" + this.getTable() + "` (" + "`" + NODE_ID
				+ "` int(8) NOT NULL," + "PRIMARY KEY (`" + NODE_ID + "`)"
				+ ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		try {
			PreparedStatement ps = this.getConnection().prepareStatement(sql);
			ps.execute();
			ps.close();
			this.getConnection().commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Node getBean(SqlInteger<Integer> id) throws SQLException {
		List<Node> list = this.select("where " + NODE_ID + "=" + id.getContent());
		if (list.size() == 0)
			return null;
		else
			return list.get(0);
	}
}
