/**
 * Copyright 2015年4月3日 Wang Zheng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Wang Zheng ufo5260987423@163.com
 *
 */
package com.ufo5260987423.graphDatabase.core.conf;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.ufo5260987423.graphDatabase.core.connector.NetConnector;
import com.ufo5260987423.graphDatabase.core.connector.REPL_Connector;
import com.ufo5260987423.graphDatabase.core.connector.SqlConnector;

/**
 * @ClassName: Context
 * @Description: TODO
 * @author Wang Zheng ufo5260987423@163.com
 * @date 2015年4月3日 下午12:15:45
 * 
 */
public class Context implements Runnable{
    private SqlConnector sqlConnector;
    private NetConnector netConnector;
    private REPL_Connector repl_Connector;

    public Context(String[]args) {
        this.loadConf(args);
    }

	public static void main(String[] args) {
        Runnable daemon=new Context(args);
        Thread thread=new Thread(daemon);
        thread.setDaemon(true);
        thread.run();
    }

    public void loadConf(String[] args){
        String confString = "core/src/main/resources/applicationContext.xml";
        if (args.length != 1) {
            System.out.println("Usage: <confFile>");
        } else
            confString = args[0];

        System.out.println("now use " + confString);
        @SuppressWarnings("resource")
        ApplicationContext atx = new FileSystemXmlApplicationContext(confString);
        this.setSqlConnector((SqlConnector) atx.getBean("sqlConnector"));
        this.setNetConnector((NetConnector) atx.getBean("netConnector"));
        this.setRepl_Connector((REPL_Connector) atx.getBean("repl_connector"));

        this.getNetConnector().setRepl_Connector(repl_Connector);

        this.getSqlConnector().loadConf();
        this.getNetConnector().loadConf();
        this.getRepl_Connector().loadConf();
    }

    @Override
    public void run() {
        this.getSqlConnector().run();
        this.getNetConnector().start();
        this.getRepl_Connector().run();

        try {
            this.getNetConnector().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public REPL_Connector getRepl_Connector() {
        return repl_Connector;
    }

    public void setRepl_Connector(REPL_Connector repl_Connector) {
        this.repl_Connector = repl_Connector;
    }

    public NetConnector getNetConnector() {
        return netConnector;
    }

    public void setNetConnector(NetConnector netConnector) {
        this.netConnector = netConnector;
    }

    public SqlConnector getSqlConnector() {
        return sqlConnector;
    }

    public void setSqlConnector(SqlConnector sqlConnector) {
        this.sqlConnector = sqlConnector;
    }
}
