(ns core.route
  (:require [ketamine.core :as ketama])
  (:use core.gao))

(println "load core.route")

(defn get-server [data servers]
  (ketama/get-node @servers (str data)))

(defn get-consistent-graph-node-id [servers local]
  (loop [id (get-new-graph-node-id)]
    (if (= local (get-server id servers))
      id
      (recur (get-new-graph-node-id)))))

(defn graph-node-is-raw? [graph-node]
  (nil? (:graph-node-id graph-node)))

(defn route [graph-node servers]
  (get-server
    (if (graph-node-is-raw? graph-node)
      (str graph-node (rand))
      (:graph-node-id graph-node))
    servers))

(defn create-servers [server-vec]
  (atom (ketama/make-ring server-vec)))
