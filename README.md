#GraphDatabase
立志做分布式图数据库

##Release特性

* 0.0.1版本
    * 支持图模型，提供对GraphNode和GraphEdge的增改删查操作。
    * 支持Clojure作为图操作的拓展命令。
    * 存在分布式能力。

##TODO

* 编写ClassLoader以解决eval问题。目前由于Jdk1.8以下的版本ClassLoader不支持对加载类空间的回收，所以理论上存在加载类上限——当然，目前并没有存在什么大的问题。
* 解决java.lang.RuntimeException: Method code too large!这个问题。这是由于加载GraphNode的时候自动加载GraphEdge，而GraphEdge太多导致的。下一个阶段可能通过建立索引与Lazy Sequence的方法来解决。
* 解决网络协议的可靠性问题
* 在2015.6.28以后释放更多基础原理的文档。

##使用说明

* 对子项目core和client分别运行mvn package命令可以得到服务端和客户端的可部署jar包。
* 对core项目可以通过spring 配置文件来设定具体配置。

##为啥要做图数据库

* 图模型在机器学习当中的应用。
* 现有的图数据库要么不支持MySQL等传统关系型数据库作为存储后端（backend），要么是社区版和企业版存在差距。
* 尝试在MySQL这种对XA存在支持bug的关系型数据库上实现分布式存储。

##关于作者

* 捐助：支付宝ufo5260987423@163.com
